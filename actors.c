#include "hashfn.h"
#include "util.h"
#include "libaries.h"
int key2val(FILE *fp, FILE *khs, char *search, char *val)
{
    //declaring variables
    int capacity = 0;
    int hash = 0;
    int index = 0;
    int i = 0;
    char *key = malloc(STRLEN);
    //getting hash and and index
    capacity = get_capacity(khs);
    hash = hashfn(search, capacity);
    read_index(khs, hash, &index);
    read_key(fp, index, key);

    i = 0;
    //then find where in the hashmap the key is
    while (strcmp(search, key) != 0 && i < capacity)
    {
        hash++;
        if (hash == capacity)
        {
            hash = 0;
        }
        read_index(khs, hash, &index);
        read_key(fp, index, key);
        i++;
    }
    free(key);
    if (i == capacity)
    {
        //if key is not found
        val = NULL;
        return 0;
    }
    else
    {
        //if key is found
        read_val(fp, index, val);
        return hash;
    }
}
void val2key(FILE *fp, FILE *vhs, char *search, char *key)
{
    //declaring
    int capacity = 0;
    int hash = 0;
    int index = 0;
    int i = 0;
    char *val = malloc(STRLEN);
    capacity = get_capacity(vhs);
    hash = hashfn(search, capacity);
    read_index(vhs, hash, &index);
    read_val(fp, index, val);
    i = 0;
    //goes through and searches for val
    while (strcmp(search, val) != 0 && i < capacity)
    {
        hash++;
        if (hash == capacity)
        {
            hash = 0;
        }
        read_index(vhs, hash, &index);
        read_val(fp, index, val);

        i++;
    }
    if (i == capacity)
    {
        //if not found
        key = NULL;
    }
    else
    {
        //if it is found
        read_key(fp, index, key);
    }
    free(val);
}
int main(int argc, char **argv)
{
    //firsly get the key value for the movie
    FILE *movies, *movievhs, *principals, *principalskhs, *actors, *actorskhs;
    char *val = malloc(256);
    char *key = malloc(256);
    char *moviekey = malloc(256);
    char *loop = malloc(256);
    int hash = 0;
    int capacity;
    int index = 0;
    //opens all the files needed
    movies = fopen("title.basics.kv", "rb");
    movievhs = fopen("title.basics.vhs", "rb");
    principals = fopen("title.principals.kv", "rb");
    principalskhs = fopen("title.principals.khs", "rb");
    actors = fopen("name.basics.kv", "rb");
    actorskhs = fopen("name.basics.khs", "rb");
    //gets tm of movie
    val2key(movies, movievhs, argv[1], moviekey);
    //get nm of first actor
    hash = key2val(principals, principalskhs, moviekey, val);
    //then prints out the actors names in row will the same tm appears
    capacity = get_capacity(principalskhs);
    read_index(principalskhs, hash, &index);
    read_key(principals, index, loop);
    while (strcmp(moviekey, loop) == 0)
    {
        key2val(actors, actorskhs, val, key);
        printf("%s\n", key);
        hash++;
        read_index(principalskhs, hash, &index);
        read_key(principals, index, loop);
        read_val(principals, index, val);
    }

    //frees all the variable
    fclose(movies);
    fclose(movievhs);
    fclose(principals);
    fclose(principalskhs);
    fclose(actors);
    fclose(actorskhs);
    free(val);
    free(key);
    free(moviekey);
    free(loop);
    return 0;
}