#include "hashfn.h"
#include "util.h"
#include "libaries.h"
int main(int argc, char **argv)
{
    int i = 0;
    int hash = 0;
    int valueinfile = 0;
    int j = 0;
    FILE *fp, *vhs, *khs;
    int capacity = 0;
    char *key = malloc(STRLEN);
    char *value = malloc(STRLEN);
    //if three command line arguments are not given
    if (argc != 3)
    {
        fprintf(stderr, "Usage: %sfilename.kv capacity\n", argv[0]);
        exit(0);
    }
    capacity = atoi(argv[2]);
    //making khs file name and vhs file name
    char *khsname = malloc(strlen(argv[1]) + 2);
    char *vhsname = malloc(strlen(argv[1]) + 2);
    strcpy(khsname, argv[1]);

    strcpy(vhsname, argv[1]);
    i = strlen(argv[1]);

    while (argv[1][i] != '.' && i >= 0)
    {
        i--;
    }
    khsname[i + 1] = 'k';
    khsname[i + 2] = 'h';
    khsname[i + 3] = 's';
    khsname[i + 4] = 0;
    vhsname[i + 1] = 'v';
    vhsname[i + 2] = 'h';
    vhsname[i + 3] = 's';
    vhsname[i + 4] = 0;
    //opening the file
    fp = fopen(argv[1], "rb");
    vhs = fopen(vhsname, "wb+");
    khs = fopen(khsname, "wb+");
    //making the khs and vhs empty
    write_empty(vhs, capacity);
    write_empty(khs, capacity);
    //writing to the vhs and khs files

    i = 0;
    while (read_keyval(fp, key, value) == 2)
    {
        //key
        hash = hashfn(key, capacity);
        read_index(khs, hash, &valueinfile);
        while (valueinfile != -1)
        {
            hash++;
            if (hash == capacity)
            {
                hash = 0;
            }
            read_index(khs, hash, &valueinfile);
        }
        write_index(khs, i, hash);
        //for value
        hash = hashfn(value, capacity);
        read_index(vhs, hash, &valueinfile);
        while (valueinfile != -1)
        {
            hash++;
            //if hash is equal to capacity
            if (hash == capacity)
            {
                hash = 0;
            }
            read_index(vhs, hash, &valueinfile);
        }
        write_index(vhs, i, hash);
        i++;
    }
    //freeing and closing files
    fclose(fp);
    fclose(vhs);
    fclose(khs);
    free(vhsname);
    free(khsname);
    free(value);
    free(key);

    return 0;
}
