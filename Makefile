
all: buildidx key2val val2key actors bacon1

buildidx:buildidx.o hashfn.o util.o
	gcc -std=c99 -Wall -pedantic buildidx.o hashfn.o util.o -o buildidx
key2val:key2val.o  hashfn.o util.o
	gcc -std=c99 -Wall -pedantic key2val.o hashfn.o util.o -o key2val
val2key:val2key.o hashfn.o util.o
	gcc -std=c99 -Wall -pedantic val2key.o hashfn.o util.o -o val2key
actors:actors.o  hashfn.o util.o
	gcc -std=c99 -Wall -pedantic actors.o hashfn.o util.o -o actors
bacon1:bacon1.o  hashfn.o util.o
	gcc -std=c99 -Wall -pedantic bacon1.o hashfn.o util.o -o bacon1









clean:
	rm buildidx buildidx.o key2val key2val.o val2key val2key.o actors actors.o bacon1 bacon1.o hashfn.o util.o
