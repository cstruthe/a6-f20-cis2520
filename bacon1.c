#include "hashfn.h"
#include "util.h"
#include "libaries.h"
int key2val(FILE *fp, FILE *khs, char *search, char *val)
{
    //declaring variables
    int capacity = 0;
    int hash = 0;
    int index = 0;
    int i = 0;
    char *key = malloc(STRLEN);
    //getting hash and and index
    capacity = get_capacity(khs);
    hash = hashfn(search, capacity);
    read_index(khs, hash, &index);
    read_key(fp, index, key);

    i = 0;
    //then find where in the hashmap the key is
    while (strcmp(search, key) != 0 && i < capacity)
    {
        hash++;
        if (hash == capacity)
        {
            hash = 0;
        }
        read_index(khs, hash, &index);
        read_key(fp, index, key);
        i++;
    }
    free(key);
    if (i == capacity)
    {
        //if key is not found
        val = NULL;
        return 0;
    }
    else
    {
        //if key is found
        read_val(fp, index, val);
        return hash;
    }
}
void val2key(FILE *fp, FILE *vhs, char *search, char *key)
{
    //declaring
    int capacity = 0;
    int hash = 0;
    int index = 0;
    int i = 0;
    char *val = malloc(STRLEN);
    capacity = get_capacity(vhs);
    hash = hashfn(search, capacity);
    read_index(vhs, hash, &index);
    read_val(fp, index, val);
    i = 0;
    //goes through and searches for val
    while (strcmp(search, val) != 0 && i < capacity)
    {
        hash++;
        if (hash == capacity)
        {
            hash = 0;
        }
        read_index(vhs, hash, &index);
        read_val(fp, index, val);

        i++;
    }
    if (i == capacity)
    {
        //if not found
        key = NULL;
    }
    else
    {
        //if it is found
        read_key(fp, index, key);
    }
    free(val);
}
int main(int argc, char **argv)
{
    //delcaring variables
    int i = 0;
    int hash = 0;
    int index;
    int capacity;
    char *key = malloc(STRLEN);
    char *val = malloc(STRLEN);
    FILE *actors, *actorsvhs, *actorskhs, *principals, *principalsvhs, *principalskhs, *movies, *movieskhs;
    char *kevin = "Kevin Bacon";
    char *kevinnm = malloc(STRLEN);
    char *moviename = malloc(STRLEN);
    char *moviekey = malloc(STRLEN);
    char *loop = malloc(STRLEN);
    //opening files
    movies = fopen("title.basics.kv", "rb");
    movieskhs = fopen("title.basics.khs", "rb");
    principals = fopen("title.principals.kv", "rb");
    principalsvhs = fopen("title.principals.vhs", "rb");
    actors = fopen("name.basics.kv", "rb");
    actorsvhs = fopen("name.basics.vhs", "rb");
    actorskhs = fopen("name.basics.khs", "rb");
    principalskhs = fopen("title.principals.khs", "rb");
    capacity = get_capacity(principalsvhs);
    //get the nm for kevin bacon
    val2key(actors, actorsvhs, kevin, kevinnm);
    //get the nm of other actor
    val2key(actors, actorsvhs, argv[1], key);
    //get the tms of other actor
    val2key(principals, principalsvhs, key, moviekey);
    key2val(movies, movieskhs, moviekey, moviename);
    hash = key2val(principals, principalskhs, moviekey, val);
    //then goes through list of actors if kevin is found it will print the movie name
    capacity = get_capacity(principalskhs);
    read_index(principalskhs, hash, &index);
    read_key(principals, index, loop);
    while (strcmp(moviekey, loop) == 0)
    {
        key2val(actors, actorskhs, val, key);
        if (strcmp(key, kevin) == 0)
        {
            printf("%s\n", moviename);
        }
        hash++;
        read_index(principalskhs, hash, &index);
        read_key(principals, index, loop);
        read_val(principals, index, val);
    }
    //freeing
    fclose(movies);
    fclose(movieskhs);
    fclose(principals);
    fclose(principalsvhs);
    fclose(principalskhs);
    fclose(actors);
    fclose(actorsvhs);
    fclose(actorskhs);
    free(key);
    free(kevinnm);
    free(val);
    free(moviename);
    free(moviekey);
    free(loop);

    return 0;
}