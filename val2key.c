#include "hashfn.h"
#include "util.h"
#include "libaries.h"
int main(int argc, char **argv)
{
    //declaring variables
    FILE *fp, *vhs;
    int i = 0;
    int hash = 0;
    int capacity = 0;
    int index = 0;
    char *val = malloc(256); 
    //if two command line arguments are not given
    if (argc != 3)
    {
        fprintf(stderr, "Usage: %s filename.kv ‘search term’\n", argv[0]);
        exit(0);
    }
    char *vhsname = malloc(strlen(argv[1]) + 2);
    strcpy(vhsname, argv[1]);
    i = strlen(argv[1]);
    while (argv[1][i] != '.' || i == strlen(argv[1]))
    {
        i--;
    }
    //making the vhs file
    vhsname[i + 1] = 'v';
    vhsname[i + 2] = 'h';
    vhsname[i + 3] = 's';
    vhsname[i + 4] = 0;
    //opening the vhs file
    fp = fopen(argv[1], "rb");
    vhs = fopen(vhsname, "rb");
    //gets capacity
    capacity = get_capacity(vhs);
    //calculated hash value and finds index
    hash = hashfn(argv[2], capacity);
    read_index(vhs, hash, &index);
    //then goes through the file trying to find the val
    read_val(fp, index, val);
    i = 0;
    while (strcmp(argv[2], val) != 0 && i < capacity)
    {
        hash++;
        //if hash equals capacity resets the string
        if (hash == capacity)
        {
            hash = 0;
        }
        read_index(vhs, hash, &index);
        read_val(fp, index, val);
        i++;
    }
    //if the val is not found
    if (i == capacity)
    {
        printf("not found\n");
    }
    else
    {
        //if it is found
        read_key(fp, index, val);
        printf("%s\n", val);
    }
    //freeing and closing file
    free(val);
    free(vhsname);
    fclose(fp);
    fclose(vhs);
    return 0;
}
