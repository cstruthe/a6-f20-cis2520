#include "hashfn.h"
#include "util.h"
#include "libaries.h"
int main(int argc, char **argv)
{
    //declaring variables
    FILE *fp, *khs;
    int i = 0;
    int hash = 0;
    int capacity = 0;
    int index = 0;
    char *key = malloc(256);
    //if not enough command line arguments are given
    if (argc != 3)
    {
        fprintf(stderr, "Usage: %s filename.kv ‘search term’\n", argv[0]);
        exit(0);
    }
    //making khs file name
    char *khsname = malloc(strlen(argv[1]) + 2);
    strcpy(khsname, argv[1]);
    i = strlen(argv[1]);
    while (argv[1][i] != '.' || i == strlen(argv[1]))
    {
        i--;
    }
    khsname[i + 1] = 'k';
    khsname[i + 2] = 'h';
    khsname[i + 3] = 's';
    khsname[i + 4] = 0;
    //opening files
    fp = fopen(argv[1], "rb");
    khs = fopen(khsname, "rb");
    //gets capcity and hash
    capacity = get_capacity(khs);
    hash = hashfn(argv[2], capacity);
    //then goes through the file and tries to find the key
    read_index(khs, hash, &index);
    read_key(fp, index, key);
    i = 0;
    while (strcmp(argv[2], key) != 0 && i < capacity)
    {
        hash++;
        //if hash equals capcity will reset hash
        if (hash == capacity)
        {
            hash = 0;
        }
        read_index(khs, hash, &index);
        read_key(fp, index, key);
        i++;
    }
    //if not found
    if (i == capacity)
    {
        printf("not found\n");
    }
    else
    {
        //if found
        read_val(fp, index, key);
        printf("%s\n", key);
    }
    //frees variables
    free(key);
    free(khsname);
    fclose(fp);
    fclose(khs);
    return 0;
}
